#include "hash.hpp"

#include <string>
#include <cstring>
#include <math.h>

size_t hash(size_t k)
{
	const size_t w = 32;
	const size_t p = 17;
	const size_t s = 2654435769;

	//std::size_t ret = floor(M * ((k*A) - floor(k*A)));
	//std::size_t ret = floor(((k*A) % W) / 3);
	size_t foo = (k*s) % (size_t(2) << w);
	size_t ret = foo >> p;

	return ret;
}

size_t hash1(std::string m)
{
	size_t len = m.length();

	size_t sum = 0;

	for(int i=0; i < len; i++)
	{
		sum += m[i];
	}



	size_t ret = sum >> 3;

	return ret;
}


size_t hash2(std::string m)
{

	size_t len = m.length();

	size_t sum = 0;

	for(int i=0; i < len; i++)
	{
		sum += m[i];
	}



	size_t ret = sum >> 19;

	return ret;
}


/*
   template <class dt>
   size_t hash(dt m)
   {

   }
   */
