# Simple Hash Functions

## Approach
    
    The numerical hash function accepts a size_t key as a parameter
    which then multiplies k by a large constant s, the remainder is
    then calculated by using the bit shift operator. 
    
    The two string hash functions add the sum of the ascii characters
    then uses the bit shift operator to multiply the sum by a large number.