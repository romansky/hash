#include "hash.hpp"
#include <iostream>
#include <stdlib.h>
#include <string>

int main(int argc, char* argv[])
{

	if (argc < 2) {
		std::cout << "Not enough arguments" << std::endl;
		return 1;
	}

	size_t foo = atoi(argv[1]);

	size_t bar = hash(foo);

	std::cout << bar << std::endl;

	std::string str1(argv[2]);

	std::cout << hash1(str1) << std::endl;

	std::string str2(argv[3]);

	std::cout << hash2(str2) << std::endl;
	
	return 0;
}
