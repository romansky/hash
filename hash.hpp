#ifndef HASH_H
#define HASH_H

#include <cstddef>
#include <string>

size_t hash(size_t k);
size_t hash1(std::string k);
size_t hash2(std::string k);

#endif
